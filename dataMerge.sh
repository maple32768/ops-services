if [ ! -e zabbix_backup.tar.gz ]
then
  echo "Copying latest redmine_backup file..."
  sudo cp `ls -dtr1 /mnt/smb/zabbix_backups/* | tail -1` ./zabbix_backup.tar.gz
fi
if [ ! -e redmine_mysql_backup.tar.gz ]
then
  echo "Copying latest redmine_backup file..."
  sudo cp `ls -dtr1 /mnt/smb/redmine_backups/*_mysql_* | tail -1` ./redmine_mysql_backup.tar.gz
fi
sudo docker run -d --name tempmysql -v $(pwd)/opsmysql:/var/lib/mysql:z -e MYSQL_ALLOW_EMPTY_PASSWORD=yes -e MYSQL_ROOT_PASSWORD='' mysql:5.6
sudo docker stop tempmysql
sudo docker run --rm --volumes-from tempmysql -v $(pwd):/backup:z ubuntu bash -c "cd /var/lib/mysql && tar zxvf /backup/redmine_mysql_backup.tar.gz --strip 1"
sudo docker start tempmysql
sleep 8
sudo docker exec -it tempmysql bash -c "mysqldump -p redmine > /var/lib/mysql/redmine.sql"
sudo cp ./opsmysql/redmine.sql .
sudo docker rm -fv tempmysql
sudo rm -rf ./opsmysql/*
sudo docker run -d --name tempmysql -v $(pwd)/opsmysql:/var/lib/mysql:z -e MYSQL_ALLOW_EMPTY_PASSWORD=yes -e MYSQL_ROOT_PASSWORD='' mariadb
sudo docker stop tempmysql
sudo docker run --rm --volumes-from tempmysql -v $(pwd):/backup:z ubuntu bash -c "cd /var/lib/mysql && tar zxvf /backup/zabbix_backup.tar.gz --strip 1"
sudo docker start tempmysql
sleep 8
sudo docker exec -it tempmysql bash -c "mysqldump zabbix > /var/lib/mysql/zabbix.sql"
sudo cp ./opsmysql/zabbix.sql .
sudo docker rm -fv tempmysql
sudo rm -rf ./opsmysql/*
sudo docker run -d --name tempmysql -v $(pwd)/opsmysql:/var/lib/mysql:z -e MYSQL_ALLOW_EMPTY_PASSWORD=yes -e MYSQL_ROOT_PASSWORD='' mysql:5.6
echo "start merging!"
sleep 20
sudo docker exec -it tempmysql mysql -e "create database zabbix;"
sudo docker exec -it tempmysql mysql -e "create database redmine;"
sudo cp ./redmine.sql ./opsmysql/
sudo cp ./zabbix.sql ./opsmysql/
sudo docker exec -it tempmysql bash -c "mysql redmine < /var/lib/mysql/redmine.sql"
sudo docker exec -it tempmysql bash -c "mysql zabbix < /var/lib/mysql/zabbix.sql"
sudo docker exec -it tempmysql bash -c "mysqldump --databases zabbix redmine > /var/lib/mysql/dump.sql"
sudo cp ./opsmysql/dump.sql .
sudo docker rm -fv tempmysql
sudo rm -rf ./opsmysql/*
sudo rm -rf ./zabbix.sql ./redmine.sql
