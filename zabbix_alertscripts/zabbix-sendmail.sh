curl "https://api.postmarkapp.com/email" \
  -X POST \
  -H "Accept: application/json" \
  -H "Content-Type: application/json" \
  -H "X-Postmark-Server-Token: e99f7882-524d-4da4-927a-d6964dc33fbf" \
  -d "{From: 'y4@myex.co.in', To: '$1', Subject: '$2', TextBody: '$3'}"
