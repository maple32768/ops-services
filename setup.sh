#!/bin/bash
#check files
if [ -e jenkins.war ]
then
  echo "jenkins.war founded!"
else
  sudo wget http://updates.jenkins-ci.org/download/war/2.190.1/jenkins.war
fi

if [ ! -e redmine_backup.tar.gz ]
then
  echo "Copying latest redmine_backup file..."
  sudo cp `ls -dtr1 /mnt/smb/redmine_backup/* | tail -1` /docker/ops-services/redmine_backup.tar.gz
fi

if [ ! -e jenkins_backup.tar.gz ]
then
  echo "Copying latest jenkins_backup file..."
  sudo cp `ls -dtr1 /mnt/smb/jenkins_backups/* | tail -1` /docker/ops-services/jenkins_backup.tar.gz
fi

if [ ! -e dump.sql ]
then
  echo "can not find dump.sql"
  exit
fi

#restore files
sudo tar zxvf ./jenkins_backup.tar.gz -C ./jenkins --strip 1
sudo tar zxvf ./redmine_backup.tar.gz -C ./redmine --strip 1
#restore databases
docker-compose up -d opsmysql
sleep 10
sudo docker exec -it ops-services_opsmysql_1 bash -c "mysql -p < /var/lib/mysql/dump.sql"
sudo chown 1000:1000 ./jenkins
docker-compose up -d
#fix zabbix user error
sudo docker exec -it ops-services_zabbix-web_1 apk add php7-fileinfo
docker-compose restart zabbix-web
#setup zabbix alert script environment
sudo docker exec -it ops-services_zabbix-server_1 apk add git
sudo docker exec -it ops-services_zabbix-server_1 apk add py-pip
sudo docker exec -it ops-services_zabbix-server_1 pip install -r /var/lib/zabbix/requirements.txt

